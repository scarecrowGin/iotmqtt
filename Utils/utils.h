
#ifndef UTILS_H
#define UTILS_H


typedef nx_struct packet_t {
	nx_uint8_t msg_type;
	nx_uint16_t msg_id;
	nx_uint8_t qos;
	nx_uint16_t nodeID;
	nx_uint8_t topic;
	nx_uint16_t payload;
} packet_t;


#define ACK 0
#define CONNACK 0
#define SUBACK 0
#define PUBACK 0
#define CONNECT 1
#define PUBLISH 2
#define SUBSCRIBE 3 

#define TEM_ID 0
#define HUM_ID 1
#define LUM_ID 2


enum{
AM_MY_MSG = 6,
};



#endif