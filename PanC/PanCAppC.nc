#include "utils.h"
#include "printf.h"

#define NEW_PRINTF_SEMANTICS

configuration PanCAppC {}

implementation {
	components MainC, PanCC as App;

	components new AMSenderC(AM_MY_MSG);
	components new AMReceiverC(AM_MY_MSG);
	components ActiveMessageC;
	components new TimerMilliC() as ResendTimer;
	components new TimerMilliC() as SendTimer;
	components SerialPrintfC;
    components SerialStartC;

	App.Boot -> MainC.Boot;
	App.Receive -> AMReceiverC;
  	App.AMSend -> AMSenderC;
  	App.SplitControl -> ActiveMessageC;
  	App.PacketAcknowledgements -> ActiveMessageC;
    App.Packet -> AMSenderC;
    App.ResendTimer -> ResendTimer;
    App.SendTimer -> SendTimer;
}
