#include "utils.h"
#include "printf.h"

#define NODE_MAX 8
#define PAN_ID  TOS_NODE_ID
#define RESEND_PUB 20
#define TOT_MESSAGES_AVAILABLE 50
#define MILLISECS 20

module PanCC {
	uses {
		      interface Boot;
              interface AMSend;
              interface SplitControl;
              interface Receive;
              interface PacketAcknowledgements;
              interface Packet;
              interface Timer<TMilli> as ResendTimer;
              interface Timer<TMilli> as SendTimer;
	}
}

implementation{



        uint8_t connected_nodes_n = 0;
        uint16_t connected_nodes[NODE_MAX];
        message_t messages[TOT_MESSAGES_AVAILABLE];
        uint8_t messages_qos[TOT_MESSAGES_AVAILABLE];
        uint16_t addresses[TOT_MESSAGES_AVAILABLE];
        uint8_t messages_inuse[TOT_MESSAGES_AVAILABLE];
        uint8_t messages_countdown[TOT_MESSAGES_AVAILABLE];
        uint16_t messages_id[TOT_MESSAGES_AVAILABLE];
        uint8_t messages_inuse_n = 0;
        uint16_t tem_subscribed[NODE_MAX];
        uint8_t tem_subscribed_qos[NODE_MAX];
        uint16_t lum_subscribed[NODE_MAX];
        uint8_t lum_subscribed_qos[NODE_MAX];
        uint16_t hum_subscribed[NODE_MAX];
        uint8_t hum_subscribed_qos[NODE_MAX];
        uint16_t messagesId = 0;
        



//GETMSGID//////////////////////////////////////////////////////////////////////////////////
        uint8_t getMsgId(){
            if(messagesId<200)
            messagesId++;
            else messagesId = 1;
            return messagesId;
        }
//SUBSCRIBENODETOTEMPERATURE////////////////////////////////////////////////////////////////
        void subscribeNodeToTemperature(uint16_t nodeID,uint8_t qos){

            uint8_t counter;
            for(counter = 0; counter<NODE_MAX;counter++){
                if(tem_subscribed[counter] == nodeID)
                    return;
            }
            counter = 0;
            while(tem_subscribed[counter]!=200){
                counter++;
            }
            if(counter<NODE_MAX){
                tem_subscribed_qos[counter] = qos;
                tem_subscribed[counter] = nodeID;
            }
        }
//SUBSCRIBENODETOHUMIDITY///////////////////////////////////////////////////////////////////
        void subscribeNodeToHumidity(uint16_t nodeID,uint8_t qos){
            uint8_t counter;
            for(counter = 0; counter<NODE_MAX;counter++){
                if(hum_subscribed[counter] == nodeID)
                    return;
            }
            counter = 0;
            while(hum_subscribed[counter]!=200){
                counter++;
            }
            if(counter<NODE_MAX){
                hum_subscribed_qos[counter] = qos;
                hum_subscribed[counter] = nodeID;
            }

        }
//SUBSCRIBENODETOLUMINOSITY/////////////////////////////////////////////////////////////////
        void subscribeNodeToLuminosity(uint16_t nodeID,uint8_t qos){
            uint8_t counter;
            for(counter = 0; counter<NODE_MAX;counter++){
                if(lum_subscribed[counter] == nodeID)
                    return;
            }
            counter = 0;
            while(lum_subscribed[counter]!=200){
                counter++;
            }
            if(counter<NODE_MAX){
                lum_subscribed_qos[counter] = qos;
                lum_subscribed[counter] = nodeID;
            }

        }
//SWITCHMESSAGES////////////////////////////////////////////////////////////////////////////
        void switchMessages(){
            uint8_t counter;
            uint16_t id = messages_id[0];
            uint8_t qos = messages_qos[0];
            uint16_t address = addresses[0];
            uint8_t countdown = messages_countdown[0];
            message_t p = messages[0];
            uint8_t tempInuse = messages_inuse[0];
            for(counter = 0; counter < TOT_MESSAGES_AVAILABLE-1; counter++){
                messages_id[counter] = messages_id[counter+1];
                messages_qos[counter] = messages_qos[counter+1];
                messages[counter] = messages[counter+1];
                messages_inuse[counter] = messages_inuse[counter+1];
                addresses[counter] = addresses[counter+1];
                messages_countdown[counter] = messages_countdown[counter+1];
            }
            messages_countdown[counter-1] = countdown;
            messages[counter-1] = p;
            messages_id[counter-1] = id;
            addresses[counter-1] = address;
            messages_qos[counter-1] = qos;
            messages_inuse[counter-1] = tempInuse;
        }
//ACKNOWLEDGED//////////////////////////////////////////////////////////////////////////////
 void acknowledged(uint16_t msg_id){
            uint8_t counter;
            for(counter = 0; counter < TOT_MESSAGES_AVAILABLE; counter++){
                if(messages_id[counter] == msg_id && messages_inuse[counter] == 1){

                    messages_inuse[counter] = 0;
                    messages_inuse_n--;
                   // printf("[DEBUG PANC] messages in use: %u\n", messages_inuse_n);
                }

                    
            }
        }
//GETFREEMESSAGE////////////////////////////////////////////////////////////////////////////
        uint8_t getFreeMessage(){
            uint8_t i;
            for(i = 0; i < TOT_MESSAGES_AVAILABLE; i++){
                if(messages_inuse[i] == 0){
                    return i;
                }
            }
            printf("[PANC] Message buffer full! \n");
            return 255;
        }
//SENDMESSAGE///////////////////////////////////////////////////////////////////////////////
        void sendMessage(uint16_t address, uint8_t msg_type, uint16_t msg_id, uint8_t qos, uint8_t topic, uint16_t payload){
            packet_t* packet;
            uint8_t message_pos = getFreeMessage();
            if(message_pos != 255){
                packet = call Packet.getPayload(&messages[message_pos],sizeof(packet_t));
                packet->msg_type = msg_type;
                packet->msg_id = msg_id;
                messages_id[message_pos] = msg_id;
                packet->qos = qos;
                messages_qos[message_pos] = qos;
                packet->nodeID = PAN_ID;
                packet->topic = topic;
                packet->payload = payload;
                addresses[message_pos] = address;
                messages_inuse[message_pos] = 1;            
                messages_inuse_n++;
                messages_countdown[message_pos] = 0;
               // printf("[DEBUG PANC] messages in use: %u (sendmessage)\n", messages_inuse_n); 
            }
            
        }
//PUBLISHHANDLER////////////////////////////////////////////////////////////////////////////
        void publishHandler(uint16_t nodeID, uint8_t topic, uint16_t payload){
            uint8_t counter;
            switch(topic){
                case TEM_ID:
                    for(counter = 0; counter < NODE_MAX; counter ++){
                        if(tem_subscribed[counter] != nodeID && tem_subscribed[counter] != 200){
                            printf("[PANC] Sending publish data to node:%u\n",tem_subscribed[counter]);
                            sendMessage(tem_subscribed[counter],PUBLISH,getMsgId(),tem_subscribed_qos[counter],TEM_ID,payload);
                        }
                    }
                    break;
                case HUM_ID:
                    for(counter = 0; counter < NODE_MAX; counter ++){
                        if(hum_subscribed[counter] != nodeID && hum_subscribed[counter] != 200){
                            printf("[PANC] Sending publish data to node:%u\n",hum_subscribed[counter]);
                            sendMessage(hum_subscribed[counter],PUBLISH,getMsgId(),hum_subscribed_qos[counter],HUM_ID,payload);
                        }
                    }
                    break;
                case LUM_ID:
                    for(counter = 0; counter < NODE_MAX; counter ++){
                        if(lum_subscribed[counter] != nodeID && lum_subscribed[counter] != 200){
                            printf("[PANC] Sending publish data to node:%u\n",lum_subscribed[counter]);
                            sendMessage(lum_subscribed[counter],PUBLISH,getMsgId(),lum_subscribed_qos[counter],LUM_ID,payload);
                        }
                    }
                    break;
            }
        }
//BOOTED////////////////////////////////////////////////////////////////////////////////////
        event void Boot.booted(){
            uint8_t i;
            for(i = 0; i < TOT_MESSAGES_AVAILABLE; i++){
                messages_countdown[i] = 0;
                messages_inuse[i] = 0;
                messages_qos[i] = 0;
                messages_id[i] = 0;
            }
            for(i = 0; i < NODE_MAX; i++){
                hum_subscribed[i] = 200;
                lum_subscribed[i] = 200;
                tem_subscribed[i] = 200;
                lum_subscribed_qos[i] = 0;
                hum_subscribed_qos[i] = 0;
                tem_subscribed_qos[i] = 0;
            }

            call SplitControl.start();
        }

//STARTDONE/////////////////////////////////////////////////////////////////////////////////
        event void SplitControl.startDone(error_t error){
            if(error == SUCCESS){
                printf("[PANC] Coordinator Online!\n");
                call SendTimer.startPeriodic(MILLISECS); 
            }
            else{
                call SplitControl.start();
            }
        }

//SENDDONE//////////////////////////////////////////////////////////////////////////////////
        event void AMSend.sendDone(message_t* buf,error_t err){}
//RESENDTIMERFIRED//////////////////////////////////////////////////////////////////////////
        event void ResendTimer.fired(){}
//SENDTIMERFIRED/////////////////////////////////////////////////////////////////////
        event void SendTimer.fired(){
            uint8_t counter = 0;
            uint8_t flag = 0;
            for(counter = 0; counter < TOT_MESSAGES_AVAILABLE; counter++){
                if(messages_inuse[counter] == 1){
                    //printf("[PANC DEBUG] found a message to send\n");
                    if(messages_countdown[counter] == 0 && flag == 0){
                        flag = 1;
                        messages_countdown[counter] = 50;
                        if(call AMSend.send(addresses[counter],&messages[counter],sizeof(packet_t))==SUCCESS){
                            //printf("[NODE %u DEBUG] sent message with type: %u and qos = %u\n",NODE_ID,packet->msg_type,packet->qos);
                        }
                        else{
                            printf("[PANC] Failed to send message to node:\n");
                        }
                        if(messages_qos[counter] == 0){
                            messages_inuse[counter] = 0;
                            messages_inuse_n--;
                         //   printf("[DEBUG PANC] messages in use: %u\n", messages_inuse_n);
                        }
                        switchMessages();
                    }
                    else if(messages_countdown[counter] > 0) messages_countdown[counter]--;
                }
            }
        }
//STOPDONE//////////////////////////////////////////////////////////////////////////////////
        event void SplitControl.stopDone(error_t error){
            printf("[PANC] Coordinator down, error n.:%u\n", error);
        }

//RECEIVE///////////////////////////////////////////////////////////////////////////////////
        event message_t* Receive.receive(message_t* buf, void* payload, uint8_t len){
            packet_t* received = (packet_t*)payload;
            uint8_t counter;
            uint16_t node = 0;
            
            //----->CONNECT<-----//
            if(received->msg_type == CONNECT){
                if(connected_nodes_n < NODE_MAX){
                    for(counter = 1; counter <= connected_nodes_n; counter++){
                        if(connected_nodes[counter-1] == received->nodeID){
                            node = counter;
                        }
                    }

                    if(node == 0){
                        printf("[PANC] Node with ID: %u connected\n",received->nodeID);
                        connected_nodes_n++;
                        connected_nodes[connected_nodes_n-1] = received->nodeID;

                        printf("[PANC] Sending connection ACK to node:%u\n",received->nodeID);
                        sendMessage(received->nodeID,CONNACK,received->msg_id,0,0,0);
                    } 
                    else{
                        sendMessage(received->nodeID,CONNACK,received->msg_id,0,0,0);
                        printf("[PANC] Node with ID: %u tried to connect, but already connected\n",received->nodeID );
                    }
                }
                else
                    printf("[PANC] Can't accept new connection from node: %u, list full\n",received->nodeID);
                    sendMessage(received->nodeID,CONNACK,received->msg_id,0,0,0);
            }
            //----->SUBSCRIBE<-----//
            
            if(received->msg_type == SUBSCRIBE){
                switch(received->topic){
                    //printf("[PANC DEBUG] choosing subscribe. topic: %u\n",received->topic);
                    case TEM_ID:
                        subscribeNodeToTemperature(received->nodeID,received->payload);break;
                    case HUM_ID:
                        subscribeNodeToHumidity(received->nodeID,received->payload);break;
                    case LUM_ID:
                        subscribeNodeToLuminosity(received->nodeID,received->payload);break;
                    default:
                        printf("[PANC] received a bad request for subscription from node: %u\n",received->nodeID);
                }
                printf("[PANC] Sending subscribe ACK to node:%u (packetId = %u)\n",received->nodeID,received->msg_id);
                sendMessage(received->nodeID,SUBACK,received->msg_id,0,0,0);
            }
            //----->PUBLISH<-----//
            if(received->msg_type == PUBLISH){
                printf("[PANC] Sending publish ACK to node:%u\n",received->nodeID);
                sendMessage(received->nodeID,PUBACK,received->msg_id,0,0,0);
                publishHandler(received->nodeID,received->topic,received->payload);

            }
             //----->ACK<-----//
            if(received->msg_type == ACK){
                acknowledged(received->msg_id);
            }

            return buf;
        }



}