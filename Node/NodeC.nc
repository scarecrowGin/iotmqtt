#include "utils.h"
#include "printf.h"

#define NODE_ID TOS_NODE_ID
#define TOT_MESSAGES_AVAILABLE 20
#define TIMER_DELAY_BASE 2000
#define TIMER_RESEND (TIMER_DELAY_BASE + NODE_ID*500)
#define TIMEOUT_SENSOR_1 8000
#define TIMEOUT_SENSOR_2 15000
#define MILLISECS 20

module NodeC
{
	uses {
		interface Boot;
		interface Packet;
		interface AMSend;
		interface SplitControl;
		interface Receive;
		interface PacketAcknowledgements;
		interface Read<uint16_t> as TSens;
		interface Read<uint16_t> as HSens;
		interface Read<uint16_t> as LSens;

        interface Timer<TMilli> as DataTimer1;
        interface Timer<TMilli> as DataTimer2;
        interface Timer<TMilli> as SendTimer;
	}
}

implementation{

		message_t messages[TOT_MESSAGES_AVAILABLE];
		uint8_t messages_inuse[TOT_MESSAGES_AVAILABLE];
        uint16_t addresses[TOT_MESSAGES_AVAILABLE];
        uint8_t messages_countdown[TOT_MESSAGES_AVAILABLE];
        uint8_t messages_inuse_n = 0;
        uint16_t messages_id[TOT_MESSAGES_AVAILABLE];
        uint8_t messages_qos[TOT_MESSAGES_AVAILABLE];
		uint16_t messagesId = 0;
        uint8_t connected = 0;
        uint8_t sub1;
        uint8_t sub2;
        uint8_t pub1;
        uint8_t pub2;
        uint8_t pub1qos;
        uint8_t pub2qos;
        uint8_t sub1qos;
        uint8_t sub2qos;
        uint16_t pancAddress;
        uint16_t valuePub1;
        uint16_t valuePub2;

//SWITCHMESSAGES////////////////////////////////////////////////////////////////////////////
        void switchMessages(){
            uint8_t counter;
            uint16_t id = messages_id[0];
            uint8_t qos = messages_qos[0];
            uint16_t address = addresses[0];
            uint8_t countdown = messages_countdown[0];
            message_t p = messages[0];
            uint8_t tempInuse = messages_inuse[0];
            for(counter = 0; counter < TOT_MESSAGES_AVAILABLE-1; counter++){
                messages_id[counter] = messages_id[counter+1];
                messages_qos[counter] = messages_qos[counter+1];
                messages[counter] = messages[counter+1];
                messages_inuse[counter] = messages_inuse[counter+1];
                addresses[counter] = addresses[counter+1];
                messages_countdown[counter] = messages_countdown[counter+1];
            }
            messages_countdown[counter-1] = countdown;
            messages[counter-1] = p;
            messages_id[counter-1] = id;
            addresses[counter-1] = address;
            messages_qos[counter-1] = qos;
            messages_inuse[counter-1] = tempInuse;
        }

//ACKNOWLEDGED//////////////////////////////////////////////////////////////////////////////
        void acknowledged(uint16_t msg_id){
            uint8_t counter;
            for(counter = 0; counter < TOT_MESSAGES_AVAILABLE; counter++){
                    //printf("[NODE %u DEBUG] msg_id:%u\n",NODE_ID,messages_id[counter]);
                if(messages_id[counter] == msg_id && messages_inuse[counter] == 1){

                    messages_inuse[counter] = 0;
                    messages_inuse_n--;
                   // printf("[DEBUG PANC] messages in use: %u\n", messages_inuse_n);
                }

                    
            }
        }
//GETFREEMESSAGE////////////////////////////////////////////////////////////////////////////
        uint8_t getFreeMessage(){
            uint8_t i;
            for(i = 0; i < TOT_MESSAGES_AVAILABLE; i++){
                if(messages_inuse[i] == 0){
                    return i;
                }
            }
            printf("[NODE %u] Message buffer full!\n",NODE_ID);
            return 200;
        }

//SENDMESSAGE///////////////////////////////////////////////////////////////////////////////
        void sendMessage(uint16_t address, uint8_t msg_type, uint16_t msg_id, uint8_t qos, uint8_t topic, uint16_t payload){
            packet_t* packet;
            uint8_t message_pos = getFreeMessage();
            //printf("[NODE %u DEBUG] creating packet with type: %u and qos: %u\n",NODE_ID,msg_type,qos);
            if(message_pos != 200){
                packet = call Packet.getPayload(&messages[message_pos],sizeof(packet_t));
                packet->msg_type = msg_type;
                packet->msg_id = msg_id;
                messages_id[message_pos] = msg_id;
                packet->qos = qos;
                messages_qos[message_pos] = qos;
                packet->nodeID = NODE_ID;
                packet->topic = topic;
                packet->payload = payload;
                addresses[message_pos] = address;
                messages_inuse[message_pos] = 1;            
                messages_inuse_n++;
                messages_countdown[message_pos] = 0;
                 
               // printf("[DEBUG PANC] messages in use: %u (sendmessage)\n", messages_inuse_n); 
            }
            
        }

//CONNECT///////////////////////////////////////////////////////////////////////////////////
        void connect(){
            sendMessage(AM_BROADCAST_ADDR,CONNECT,0,1,0,0);
            call SendTimer.startPeriodic(MILLISECS); 
        }



//GETMSGID//////////////////////////////////////////////////////////////////////////////////
		uint8_t getMsgId(){
            if(messagesId<200)
            messagesId++;
            else messagesId = 1;
            return messagesId;
        }
//DATATIMER1FIRED///////////////////////////////////////////////////////////////////////////
        event void DataTimer1.fired(){
            switch(pub1){
                case 0: 
                    call TSens.read(); break;
                case 1:
                    call HSens.read(); break;
                case 2:
                    call LSens.read(); break;
            }
        }

//DATATIMER1FIRED///////////////////////////////////////////////////////////////////////////
        event void DataTimer2.fired(){
            switch(pub2){
                case 0: 
                    call TSens.read(); break;
                case 1:
                    call HSens.read(); break;
                case 2:
                    call LSens.read(); break;
            }
        }

//GENERATEPUBLISHES/////////////////////////////////////////////////////////////////////////
        void generatePublishes(){
        /*    pub1 = LocalTime.get()%3;
            pub2 = pub1;
            while(pub2 == pub1)
                pub2 = LocalTime.get()%3;
        */
            switch(NODE_ID%3){
                case 0:
                    pub1 = 0;
                    pub2 = 1;
                    break;
                case 1:
                    pub1 = 0;
                    pub2 = 2;
                    break;
                case 2:
                    pub1 = 1;
                    pub2 = 2;
                    break;
            }
        }

//GENERATESUBSCRIPTIONS/////////////////////////////////////////////////////////////////////
        void generateSubscriptions(){
        /*    sub1 = LocalTime.get()%3;
            sub2 = sub1;
            while(sub2 == sub1)
                sub2 = LocalTime.get()%3;
        */
            switch(NODE_ID%3){
                case 0:
                    sub1 = 2;
                    sub2 = 1;
                    break;
                case 1:
                    sub1 = 2;
                    sub2 = 0;
                    break;
                case 2:
                    sub1 = 1;
                    sub2 = 0;
                    break;
            }
        }

//GENERATEPUBLISHQOS////////////////////////////////////////////////////////////////////////
        void generatePublishQos(){
            switch(NODE_ID%4){
                case 0:
                    pub1qos = 0;
                    pub2qos = 0;
                    break;
                case 1:
                    pub1qos = 1;
                    pub2qos = 0;
                    break;
                case 2:
                    pub1qos = 0;
                    pub2qos = 1;
                    break;
                case 3:
                    pub1qos = 1;
                    pub2qos = 1;
                    break;
            }
        }

//GENERATESUBSCRIPTIONQOS////////////////////////////////////////////////////////////////////
        void generateSubscriptionQos(){
            switch(NODE_ID%4){
                case 0:
                    sub1qos = 1;
                    sub2qos = 1;
                    break;
                case 1:
                    sub1qos = 0;
                    sub2qos = 1;
                    break;
                case 2:
                    sub1qos = 1;
                    sub2qos = 0;
                    break;
                case 3:
                    sub1qos = 0;
                    sub2qos = 0;
                    break;
            }
        }

//SUBSCRIBE/////////////////////////////////////////////////////////////////////////////////
        void subscribe(){
            sendMessage(pancAddress,SUBSCRIBE,getMsgId(),1,sub1,sub1qos);
            sendMessage(pancAddress,SUBSCRIBE,getMsgId(),1,sub2,sub2qos);
            printf("[NODE %u] Node subscribed to %u and %u using PANC address %u\n",NODE_ID, sub1, sub2,pancAddress);
        }


//BOOTED////////////////////////////////////////////////////////////////////////////////////
        event void Boot.booted(){
            uint8_t i;
        	for(i = 0; i < TOT_MESSAGES_AVAILABLE; i++){
                messages_inuse[i] = 0;
                messages_countdown[i] = 0;
                messages_qos[i] = 0;
                messages_id[i] = 0;
            }

            generateSubscriptions();
            generatePublishes();
            generatePublishQos();
            generateSubscriptionQos();
        	call SplitControl.start();
        }

//STARTDONE/////////////////////////////////////////////////////////////////////////////////
        event void SplitControl.startDone(error_t error){
            if(error == SUCCESS){
                printf("[NODE %u] Node %u Online! Connecting to PANC...\n",NODE_ID,NODE_ID);
                connect();
            }
            else{
                call SplitControl.start();
            }
        }
//SENDTIMERFIRED////////////////////////////////////////////////////////////////////////////
        event void SendTimer.fired(){
            uint8_t counter = 0;
            uint8_t flag = 0;
            for(counter = 0; counter < TOT_MESSAGES_AVAILABLE; counter++){
                if(messages_inuse[counter] == 1){
                    if(messages_countdown[counter] == 0 && flag == 0){
                        flag = 1;
                        messages_countdown[counter] = 50;
                        if(call AMSend.send(addresses[counter],&messages[counter],sizeof(packet_t))==SUCCESS){

                            //printf("[NODE %u DEBUG] sent message with type: %u and qos = %u\n",NODE_ID,packet->msg_type,packet->qos);
                        }
                        else{
                            printf("[NODE %u] Failed to send message to PANC\n",NODE_ID);
                        }
                        if(messages_qos[counter] == 0){
                            //printf("[NODE %u DEBUG] removed a message\n",NODE_ID);
                            messages_inuse[counter] = 0;
                            messages_inuse_n--;
                            //   printf("[DEBUG PANC] messages in use: %u\n", messages_inuse_n);
                        }
                        
                        switchMessages();
                    }
                    else if(messages_countdown[counter] > 0) messages_countdown[counter]--;
                }
            }
        }
//SENDDONE//////////////////////////////////////////////////////////////////////////////////
        event void AMSend.sendDone(message_t* buf,error_t err){}

//STOPDONE//////////////////////////////////////////////////////////////////////////////////
        event void SplitControl.stopDone(error_t error){
            printf("[NODE %u] Node %u down, error n.:%u\n", NODE_ID,NODE_ID,error);
        }
//HSENS.READDONE////////////////////////////////////////////////////////////////////////////
        event void HSens.readDone(error_t result, uint16_t data){
            uint8_t qos = 0;
            if((pub1 == HUM_ID && pub1qos == 1) || (pub2 == HUM_ID && pub1qos == 1)) qos = 1;
            if(result == SUCCESS){
                printf("[NODE %u] new data available on HUMIDITY(%u): %u, sending with qos %u\n",NODE_ID,HUM_ID,data,qos);
                sendMessage(pancAddress,PUBLISH,getMsgId(),qos,HUM_ID,data);
            }
        }
//LSENS.READDONE////////////////////////////////////////////////////////////////////////////
        event void LSens.readDone(error_t result, uint16_t data){
            uint8_t qos = 0;
            if((pub1 == LUM_ID && pub1qos == 1) || (pub2 == LUM_ID && pub1qos == 1)) qos = 1;
            if(result == SUCCESS){
                printf("[NODE %u] new data available on LUMINOSITY(%u): %u, sending with qos %u\n",NODE_ID,LUM_ID,data,qos);
                sendMessage(pancAddress,PUBLISH,getMsgId(),qos,LUM_ID,data);
            }

        }
//TSENS.READDONE////////////////////////////////////////////////////////////////////////////
        event void TSens.readDone(error_t result, uint16_t data){
            uint8_t qos = 0;
            if((pub1 == TEM_ID && pub1qos == 1) || (pub2 == TEM_ID && pub1qos == 1)) qos = 1;
            if(result == SUCCESS){
                printf("[NODE %u] new data available on TEMPERATURE(%u): %u, sending with qos %u\n",NODE_ID,TEM_ID,data,qos);
                sendMessage(pancAddress,PUBLISH,getMsgId(),qos,TEM_ID,data);
            }

        }
//RECEIVE///////////////////////////////////////////////////////////////////////////////////
        event message_t* Receive.receive(message_t* buf, void* payload, uint8_t len){
            packet_t* received = (packet_t*)payload;

            if(received->msg_type == PUBLISH){
                printf("[NODE %u] received Publish on topic %u with value: %u\n",NODE_ID,received->topic,received->payload);
                sendMessage(pancAddress,PUBACK,received->msg_id,0,0,0);
            }
            else if(received->msg_type == ACK){
                if(connected == 0){
                    if(received->msg_id == 0){
                        connected = 1; 
                        printf("[NODE %u] Connected to PANC\n",NODE_ID);
                        pancAddress = received->nodeID;
                        subscribe();
                        acknowledged(0);
                        call DataTimer1.startPeriodic(TIMEOUT_SENSOR_1);
                        call DataTimer2.startPeriodic(TIMEOUT_SENSOR_2);
                    }
                }
                acknowledged(received->msg_id);


            }




            return buf;
        }







}