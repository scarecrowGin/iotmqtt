#include "utils.h"
#include "printf.h"

#define NEW_PRINTF_SEMANTICS


configuration NodeAppC{}

implementation {

    components MainC,NodeC as App;
    components ActiveMessageC;
    
    components new AMSenderC(AM_MY_MSG);
    components new AMReceiverC(AM_MY_MSG);
    components new FakeSensorC() as TSens;
    components new FakeSensorC() as HSens;
    components new FakeSensorC() as LSens;
    components new TimerMilliC() as DataTimer1;
    components new TimerMilliC() as DataTimer2;
    components new TimerMilliC() as SendTimer;

    

    //printf components
    components SerialPrintfC;
    components SerialStartC;


    App.Boot -> MainC;
    App.Receive -> AMReceiverC;
    App.AMSend -> AMSenderC;
    App.SplitControl -> ActiveMessageC;
    App.PacketAcknowledgements -> ActiveMessageC;
    App.Packet -> AMSenderC;
    App.TSens -> TSens;
    App.HSens -> HSens;
    App.LSens -> LSens;
    App.DataTimer1 -> DataTimer1;
    App.DataTimer2 -> DataTimer2;
    App.SendTimer -> SendTimer;


}
